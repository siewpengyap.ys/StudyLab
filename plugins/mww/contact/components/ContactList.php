<?php namespace Mww\Contact\Components;

use Cms\Classes\ComponentBase;

/**
 * ContactList Component
 */
class ContactList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'ContactList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
