<?php namespace Mww\Contact\Components;

use Cms\Classes\ComponentBase;
use Mww\Contact\Models\Contact;

/**
 * ContactUs Component
 */
class ContactUs extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'ContactUs Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['title'] = 'My Contact Us';
    }

    public function getTitle()
    {
        return 'My Contact Us2';
    }

    public function onContactUsSubmit()
    {
        $model = new Contact;
        $model->fill(post());
        $model->save();
    }
}
