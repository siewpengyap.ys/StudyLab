<?php namespace Mww\Contact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateContactsTable Migration
 */
class AddPhoneToContactsTable extends Migration
{
    public function up()
    {
        Schema::table('mww_contact_contacts', function (Blueprint $table) {
            $table->string('phone')->after('email');
        });
    }

    public function down()
    {
        Schema::table('mww_contact_contacts', function (Blueprint $table) {
            $table->dropColumn('phone');
        });
    }
}
