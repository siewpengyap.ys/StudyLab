<?php namespace Mww\Contact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateContactsTable Migration
 */
class CreateContactsTable extends Migration
{
    public function up()
    {
        Schema::create('mww_contact_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->text('message');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mww_contact_contacts');
    }
}
