<?php namespace Spyap\Contact\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateContactsTable Migration
 */
class AddFbToContactsTable extends Migration
{
    public function up()
    {
        Schema::table('spyap_contact_contacts', function (Blueprint $table) {
            $table->string('fb')->nullable();
            $table->text('bio')->nullable();
        });
    }

    public function down()
    {
        Schema::table('spyap_contact_contacts', function (Blueprint $table) {
            $table->dropColum('fb')->nullable();
            $table->dropColum('bio')->nullable();
        });
    }
}
