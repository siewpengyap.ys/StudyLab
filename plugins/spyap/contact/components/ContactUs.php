<?php namespace Spyap\Contact\Components;

use Cms\Classes\ComponentBase;
use spyap\Contact\Models\Contact;

/**
 * ContactUs Component
 */
class ContactUs extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'ContactUs Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['title'] = 'My Contact Us - On Run';
    }

    public function getTitle()
    {
        return 'My Contact Us - Get Title';
    }

    public function onSubmit()
    {
        $model = new Contact;
        $model->fill(post());
        $model->save();
    }
}
