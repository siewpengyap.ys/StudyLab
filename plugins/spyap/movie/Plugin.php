<?php namespace Spyap\Movie;

use Backend;
use System\Classes\PluginBase;

/**
 * Movie Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Movie',
            'description' => 'No description provided yet...',
            'author'      => 'spyap',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'Spyap\Movie\Components\MovieList' => 'movielist',
            'Spyap\Movie\Components\MovieDetails' => 'moviedetails',
            'Spyap\Movie\Components\GenreDetails' => 'genredetails',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        //return []; // Remove this line to activate
        //'spyap.movie.some_permission'

        return [
            'spyap.movie.manage_movie' => [
                'tab' => 'Movie',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'modules' => [
                'label'       => 'Modules',
                'url'         => Backend::url('spyap/movie/movies'),
                'icon'        => 'icon-leaf',
                'permissions' => ['spyap.movie.*'],
                'order'       => 500,
                'sideMenu' => [
                    'movie' => [
                        'label'       => 'Movie',
                        'icon'        => 'icon-video-camera',
                        'url'         => Backend::url('spyap/movie/movies'),
                        'permissions' => ['spyap.movie.*'],
                    ],
                    'genre' => [
                        'label'       => 'Genre',
                        'icon'        => 'icon-copy',
                        'url'         => Backend::url('spyap/movie/genres'),
                        'permissions' => ['spyap.movie.*'],
                    ],
                    'actor' => [
                        'label'       => 'Actor',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('spyap/movie/actors'),
                        'permissions' => ['spyap.movie.*'],
                    ],
                ]
            ]
        ];
    }
}
