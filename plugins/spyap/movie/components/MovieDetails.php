<?php namespace Spyap\Movie\Components;

use Cms\Classes\ComponentBase;
use spyap\Movie\Models\Movie;

/**
 * MovieDetails Component
 */
class MovieDetails extends ComponentBase
{
    public $details;

    public function componentDetails()
    {
        return [
            'name' => 'MovieDetails Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->details = $this->loadMovieDetails();
    }

    function loadMovieDetails()
    {
        $slug = $this->param('slug');
        $query = Movie::all();

        $query = $query->where('slug', $slug)->first();
        
        return $query;
    }
}