<?php namespace Spyap\Movie\Components;

use Cms\Classes\ComponentBase;
use spyap\Movie\Models\Genre;

/**
 * Genredetails Component
 */
class Genredetails extends ComponentBase
{
    public $genres;

    public function componentDetails()
    {
        return [
            'name' => 'GenreDetails Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->genres = $this->loadGenreDetails();
    }

    function loadGenreDetails()
    {
        $slug = $this->param('slug');
        $query = Genre::all();

        $query = $query->where('slug', $slug)->first();
        
        return $query;
    }
}
