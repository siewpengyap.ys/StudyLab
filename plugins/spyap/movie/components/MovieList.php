<?php namespace Spyap\Movie\Components;

use Cms\Classes\ComponentBase;
use spyap\Movie\Models\Movie;

/**
 * MovieList Component
 */
class MovieList extends ComponentBase
{
    public $movies;

    public function componentDetails()
    {
        return [
            'name' => 'MovieList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun()
    {
        $this->movies = $this->loadMovies();
    }

    function loadMovies()
    {
        /*$query = Movie::all();

        $query = $query->sortBy('year');*/

        $query = Movie::paginate(2);

        return $query;
    }
}
