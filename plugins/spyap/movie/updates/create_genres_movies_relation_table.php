<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateGenresTable Migration
 */
class CreateGenresMoviesRelationTable extends Migration
{
    public function up()
    {
        Schema::create('spyap_movie_genres_movies', function (Blueprint $table) {
            $table->integer('genre_id')->unsigned();
            $table->integer('movie_id')->unsigned();
            $table->primary(['genre_id', 'movie_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('spyap_movie_genres_movies');
    }
}
