<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateGenresTable Migration
 */
class CreateGenresTable extends Migration
{
    public function up()
    {
        Schema::create('spyap_movie_genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('genre_title');
            $table->string('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('spyap_movie_genres');
    }
}
