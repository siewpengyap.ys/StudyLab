<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateMoviesTable Migration
 */
class CreateMoviesTable extends Migration
{
    public function up()
    {
        Schema::create('spyap_movie_movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('movie');
            $table->text('description')->nullable();
            $table->integer('year')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('spyap_movie_movies');
    }
}
