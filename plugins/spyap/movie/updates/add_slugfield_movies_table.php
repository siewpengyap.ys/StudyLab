<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateMoviesTable Migration
 */
class AddSlugfieldMoviesTable extends Migration
{
    public function up()
    {
        Schema::table('spyap_movie_movies', function (Blueprint $table) {
            $table->string('slug')->after('name')->nullable();
        });
    }

    public function down()
    {
        Schema::table('spyap_movie_movies', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
