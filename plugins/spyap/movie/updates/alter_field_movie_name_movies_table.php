<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateMoviesTable Migration
 */
class AlterFieldMovieNameMoviesTable extends Migration
{
    public function up()
    {
        Schema::table('spyap_movie_movies', function (Blueprint $table) {
            $table->renameColumn('movie','name');
        });
    }

    public function down()
    {
        Schema::table('spyap_movie_movies', function (Blueprint $table) {
            $table->renameColumn('name','movie');
        });
    }
}
