<?php namespace Spyap\Movie\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * CreateActorsTable Migration
 */
class CreateActorsTable extends Migration
{
    public function up()
    {
        Schema::create('spyap_movie_actors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('spyap_movie_actors');
    }
}
